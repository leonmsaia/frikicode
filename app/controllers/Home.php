<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Internal_model');
		$this->load->model('Commercial_model');
	}
	
	public function index()
	{
		if (getSiteConfiguration()['cooming_soon'] == false) {

			// Render Info for Toolbar
			$data['services'] = $this->Internal_model->getServices();
			$data['products'] = $this->Commercial_model->getProds();
			// Render Info for Toolbar End

			// Render Info for  Body //
			$data['works'] = $this->Commercial_model->getWorks();
			$data['about'] = $this->Internal_model->getAllAbouts();
			$data['crew'] = $this->Internal_model->getCrew();
			$data['testimonial'] = $this->Internal_model->getAllTestimonials();
			// Render Info for Body End //

			// Render Title and Tags
			$data['title'] = getSiteConfiguration()['site_name'];
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];
			// Render Title and Tags End

			// Render Visualizations
			$data['titleSpot'] = 'Lo Bueno es Divertido'; 
			$data['subtextSpot'] = 'Convertimos ideas en graficos, graficos en codigo, codigo en comunicacion y comunicacion en nuevos clientes.';
			$data['imageSpot'] = '';
			$data['videoSpot'] = 'video-coding-2757.mp4';
			// Render Visualizations End

			// Get Feed from Wordpress
			$data['blogUrl'] = $this->config->item('blog_url');
			$url = $data['blogUrl'] . 'feed/feedname';
			$data['rss'] = getNews($url);
			// Get Feed from Wordpress End

			$this->load->view('home/home', $data);
		}else{
			redirect('Under', 'refresh');
		}
	}

	public function mailChimpAdd()
	{
		$mail = $_POST['emailChimpMail'];
		//echo $mail;

		$mailChimpAPI = 'c576c13549932e04007509ca7e275982-us10';
		$mailChimpListID = '969a1affb3';

	    $this->load->helper('Mailchimp');
	    $email = $this->input->post('emailChimpMail');

	    $api = new MCAPI($mailChimpAPI);
	    $email_type = 'html';
	    $merge_vars = Array( 
	        'EMAIL' => $mail
	    );
	    if($api->listSubscribe($mailChimpListID, $email, $merge_vars , $email_type) === true) {
	        echo 'Tu Mail se agrego Correctamente.';
	        redirect('Home', 'refresh');
	    }else{
	        echo '<b>Error:</b>&nbsp; ' . $api->errorMessage;
	        redirect('Home', 'refresh');
	    }


	}

}
