<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internal extends CI_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->model('Commercial_model');
		$this->load->model('Internal_model');		
	}

	public function about()
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getProds();
		// Render Info for Toolbar End
		
		// Render Body Info
		$data['about'] = $this->Internal_model->getAllAbouts();
		$data['crew'] = $this->Internal_model->getCrew();
		// Render Body Info End

		// Render Title and Tags
		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Quienes Somos';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Render Visualizations
		$data['titleSpot'] = 'Quienes Somos';
		$data['subtextSpot'] = 'Conoce a nuestro equipo de profesionales de IT, nuestro recurso mas preciado. Dia a dia nos perfeccionamos como seres humanos y como profesionales. Nuestro objetivo es desarrollador metodologias al ritmo de los nuevos desarrollos.';
		$data['imageSpot'] = 'about.jpg';
		$data['videoSpot'] = '';
		// Render Visualizations End	

		// Get Feed from Wordpress
		$data['blogUrl'] = $this->config->item('blog_url');
		$url = $data['blogUrl'] . 'feed/feedname';
		$data['rss'] = getNews($url);
		// Get Feed from Wordpress End

		// Load View
		$this->load->view('internal/about', $data);
	}

	public function crewPorfolio($behancePorfolio)
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getWorks();
		// Render Info for Toolbar End
		
		// Retrive Nfo from Internal
		$data['crewPrfl'] = $this->Internal_model->getCrewByBehanceName($behancePorfolio);
		
		// Render for Package Detail
		// Render Title and Tags

		if ($data['crewPrfl']->num_rows() > 0) {
			
			foreach ($data['crewPrfl']->result() as $crw) {
				$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . $crw->name;
			}

			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Get Behance Porfolio
			$behanceAPI = 'ks7QRwiboXVMxaivjw6PUERvjIphkADK';
			$behanceURL = 'http://www.behance.net/v2/users/' . $behancePorfolio . '/projects?api_key=' . $behanceAPI;
			// Get Behance Porfolio End

			$service_url = $behanceURL;
			$curl = curl_init($service_url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
			$curl_response = curl_exec($curl);
			curl_close($curl);
			$behancePrfl = json_decode($curl_response, true);

			// Render Visualizations
			$data['titleSpot'] = $crw->name . ' ' . $crw->lastname;
			$data['subtextSpot'] = $crw->position;
			
			$data['imageSpot'] = '';
			$data['videoSpot'] = '';
			// Render Visualizations End

			$data['personalPorfolio'] = $behancePrfl['projects'];

			// Get Feed from Wordpress
			$data['blogUrl'] = $this->config->item('blog_url');
			$url = $data['blogUrl'] . 'feed/feedname';
			$data['rss'] = getNews($url);
			// Get Feed from Wordpress End

			// Load View
			$this->load->view('internal/crewPorfolio', $data);
		}else{
			// Error 404 Page
			redirect('Error/NotFound', 'refresh');
		}
	}

	public function partners()
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getProds();
		// Render Info for Toolbar End
		
		// Render for Package Detail
		// Render Title and Tags
		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Socios, Colaboradores, Proveedores e Inspiracion';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Render Visualizations
		$data['titleSpot'] = 'Socios, Colaboradores, Proveedores e Inspiracion';
		$data['subtextSpot'] = 'Esta seccion esta dedicada a las Organizaciones que dia a dia nos brindan su apoyo de una manera u otra y con las cuales colaboramos para elaborar dia a dia un mundo mas justo para todos.';
		$data['imageSpot'] = 'charity.jpg';
		$data['videoSpot'] = '';
		// Render Visualizations End

		// Page Info Load
		$data['partners'] = $this->Internal_model->getAllPartners();
		// Page Info Load End

		// Get Feed from Wordpress
		$data['blogUrl'] = $this->config->item('blog_url');
		$url = $data['blogUrl'] . 'feed/feedname';
		$data['rss'] = getNews($url);
		// Get Feed from Wordpress End

		// Load View
		$this->load->view('internal/partners', $data);
	}

	public function getPartner($slug)
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getWorks();
		// Render Info for Toolbar End
		
		// Retrive Nfo from Internal
		$data['partner'] = $this->Internal_model->getPartnerBySlug($slug);
		
		// Render for Package Detail
		// Render Title and Tags
		if ($data['partner']->num_rows() > 0) {
			foreach ($data['partner']->result() as $prtnr) {
				$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . $prtnr->name;
			}
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Render Visualizations
			$data['titleSpot'] = $prtnr->name;
			$data['subtextSpot'] = $prtnr->short_desc;

			$data['descSpot'] = $prtnr->desc;
			$data['imgSpot'] = $prtnr->img;
			$data['urlSpot'] = $prtnr->url;
			$data['thumbSpot'] = $prtnr->thumb;
			
			$data['imageSpot'] = $data['imgSpot'];
			$data['videoSpot'] = '';
			// Render Visualizations End

			// Get Feed from Wordpress
			$data['blogUrl'] = $this->config->item('blog_url');
			$url = $data['blogUrl'] . 'feed/feedname';
			$data['rss'] = getNews($url);
			// Get Feed from Wordpress End

			// Load View
			$this->load->view('internal/partners-item', $data);
		}else{
			// Error 404 Page
			redirect('Error/NotFound', 'refresh');
		}
	}

	public function services()
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getProds();
		// Render Info for Toolbar End
		
		// Render for Package Detail
		// Render Title and Tags
		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Servicios';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Render Visualizations
		$data['titleSpot'] = 'Servicios';
		$data['subtextSpot'] = 'Disponemos una amplia gama de servicios para poner a tu disposicion, nos especializamos en soluciones punta a punta en Desarrollo de Sistemas A Medida.';

		$data['imageSpot'] = 'services.jpg';
		$data['videoSpot'] = '';
		// Render Visualizations End

		// Get Feed from Wordpress
		$data['blogUrl'] = $this->config->item('blog_url');
		$url = $data['blogUrl'] . 'feed/feedname';
		$data['rss'] = getNews($url);
		// Get Feed from Wordpress End

		// Page Info Load
		
		// Page Info Load End

		// Load View
		$this->load->view('internal/services', $data);
	}

	public function getService($slug)
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getProds();
		// Render Info for Toolbar End
		
		// Retrive Nfo from Internal
		$data['service'] = $this->Internal_model->getServiceBySlug($slug);
		
		// Render for Package Detail
		// Render Title and Tags
		if ($data['service']->num_rows() > 0) {
			foreach ($data['service']->result() as $serv) {
				$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . $serv->title;
			}
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Render Visualizations
			$data['titleSpot'] = $serv->title;
			$data['subtextSpot'] = $serv->short_desc;

			$data['descSpot'] = $serv->desc;
			$data['methodSpot'] = $serv->method;
			$data['imgSpot'] = $serv->img;

			$data['imageSpot'] = $data['imgSpot'];
			$data['videoSpot'] = '';
			// Render Visualizations End

			// Get Feed from Wordpress
			$data['blogUrl'] = $this->config->item('blog_url');
			$url = $data['blogUrl'] . 'feed/feedname';
			$data['rss'] = getNews($url);
			// Get Feed from Wordpress End

			// Load View
			$this->load->view('internal/service-item', $data);
		}else{
			// Error 404 Page
			redirect('Error/NotFound', 'refresh');
		}
	}

	public function products()
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getProds();
		// Render Info for Toolbar End
		
		// Render for Package Detail
		// Render Title and Tags
		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Productos';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Render Visualizations
		$data['titleSpot'] = 'Productos';
		$data['subtextSpot'] = 'En FrikiCode estamos obsesionados con la produccion de microservicios orientado a PyMES que necesitan modernizar o aumentar la calidad de sus operaciones diarias. Brindamos una completa suite de aplicativos orientados a regular procesos, tanto financieros, como de fidelizacion de clientes.';

		$data['imageSpot'] = 'products.jpg';
		$data['videoSpot'] = '';
		// Render Visualizations End

		// Get Feed from Wordpress
		$data['blogUrl'] = $this->config->item('blog_url');
		$url = $data['blogUrl'] . 'feed/feedname';
		$data['rss'] = getNews($url);
		// Get Feed from Wordpress End

		// Page Info Load
		
		// Page Info Load End

		// Load View
		$this->load->view('internal/products', $data);
	}

	public function getProduct($slug)
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getProds();
		// Render Info for Toolbar End
		
		// Retrive Nfo from Internal
		$data['product'] = $this->Commercial_model->getProdBySlug($slug);
		
		// Render for Package Detail
		// Render Title and Tags
		if ($data['product']->num_rows() > 0) {
			foreach ($data['product']->result() as $prod) {
				$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . $prod->name;
			}
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Render Visualizations
			$data['titleSpot'] = $prod->name;
			$data['subtextSpot'] = $prod->short_desc;

			$data['descSpot'] = $prod->desc;
			$data['methodSpot'] = $prod->method;
			$data['imgSpot'] = '';

			$data['imageSpot'] = $data['imgSpot'];
			$data['videoSpot'] = '';
			// Render Visualizations End

			// Get Feed from Wordpress
			$data['blogUrl'] = $this->config->item('blog_url');
			$url = $data['blogUrl'] . 'feed/feedname';
			$data['rss'] = getNews($url);
			// Get Feed from Wordpress End

			// Load View
			$this->load->view('internal/product-item', $data);
		}else{
			// Error 404 Page
			redirect('Error/NotFound', 'refresh');
		}
	}

	public function works()
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getProds();
		// Render Info for Toolbar End
		
		// Render for Package Detail
		// Render Title and Tags
		$data['works'] = $this->Commercial_model->getWorks();

		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Trabajos Realizados';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Render Visualizations
		$data['titleSpot'] = 'Trabajos Realizados';
		$data['subtextSpot'] = 'Conoce algunos de nuestros mejores trabajos, verifica hoy mismo la calidad de nuestro codigo y comparti nuestra vision.';

		$data['imageSpot'] = 'code.jpeg';
		$data['videoSpot'] = '';
		// Render Visualizations End

		// Get Feed from Wordpress
		$data['blogUrl'] = $this->config->item('blog_url');
		$url = $data['blogUrl'] . 'feed/feedname';
		$data['rss'] = getNews($url);
		// Get Feed from Wordpress End

		// Page Info Load
		
		// Page Info Load End

		// Load View
		$this->load->view('internal/works', $data);
	}

	public function getWork($slug)
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getWorks();

		// Render Info for Toolbar End
		
		// Retrive Nfo from Internal
		$data['work'] = $this->Commercial_model->getWorkBySlug($slug);
		
		// Render for Package Detail
		// Render Title and Tags
		if ($data['work']->num_rows() > 0) {
			foreach ($data['work']->result() as $wrkDtl) {
				$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . $wrkDtl->name;
			}
			$workid = $wrkDtl->work_id;
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Render Visualizations
			$originalDate = $wrkDtl->date;
			$newDate = date('d/m/Y', strtotime($originalDate));

			$data['titleSpot'] = $wrkDtl->name;
			$data['subtextSpot'] = $wrkDtl->short_desc;

			$data['descSpot'] = $wrkDtl->desc;
			$data['imgSpot1'] = $wrkDtl->pic1;
			$data['imgSpot2'] = $wrkDtl->pic2;
			$data['imgSpot3'] = $wrkDtl->pic3;
			$data['clientSpot'] = $wrkDtl->client;
			$data['categorySpot'] = $wrkDtl->category;
			$data['dateSpot'] = $newDate;
			$data['linkSpot'] = $wrkDtl->url;

			$data['imageSpot'] = $data['imgSpot1'];
			$data['videoSpot'] = '';
			// Render Visualizations End

			// Get Feed from Wordpress
			$data['blogUrl'] = $this->config->item('blog_url');
			$url = $data['blogUrl'] . 'feed/feedname';
			$data['rss'] = getNews($url);
			// Get Feed from Wordpress End
			
			// Calc Previus and Next Work
			$data['workNext'] = $this->Commercial_model->getNextWorkByCurrentID($workid);
			$data['workPrev'] = $this->Commercial_model->getPrevWorkByCurrentID($workid);
			// Calc Previus and Next Work End

			// Load View
			$this->load->view('internal/work-item', $data);
		}else{
			// Error 404 Page
			redirect('Error/NotFound', 'refresh');
		}
	}

}