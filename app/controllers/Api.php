<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->model('Commercial_model');
		$this->load->model('Internal_model');	
		$this->load->model('Location_Model');		
	}

	public function services() 
	{
		$serv = $this->Internal_model->getServices();  
		$infoGiver = $serv->result();
	    header('Content-Type: application/json');
	    echo json_encode($infoGiver);
	}

	public function products() 
	{
		$prod = $this->Commercial_model->getProds();
		$infoGiver = $prod->result();
	    header('Content-Type: application/json');
	    echo json_encode($infoGiver);
	}

	public function dolar()
	{
	    $headers = array(
	        'Content-Type: application/json',
	    );
	    $fields = array(
	        'format' => 'json',
	    );
	    $url = 'http://contenidos.lanacion.com.ar/json/dolar';
	    // Open connection
	    $ch = curl_init();
	    // Set the url, number of GET vars, GET data
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_POST, false);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    // Execute request
	    $result = curl_exec($ch);
	    // Close connection
	    // curl_close($ch);
	    $result = str_replace('dolarjsonpCallback({', '', $result);
	    $result = str_replace('"});', '', $result);
	    $result = explode('","', $result);
	    // $result = array_flip($result);
	    preg_match_all('!\d+!', $result[1], $casaCambioCompra);
	    $dolarCompra = $casaCambioCompra[0][0] . ',' . $casaCambioCompra[0][1];
	    preg_match_all('!\d+!', $result[5], $casaCambioVenta);
	    $dolarVenta = $casaCambioVenta[0][0] . ',' . $casaCambioVenta[0][1];
	    
	    $dolarValue = array(
	        'compra' => $dolarVenta,
	        'venta' => $dolarCompra 
	    );
	    echo json_encode($dolarValue);
	}

	function horoscope()
	{
	    $headers = array(
	        'Content-Type: application/json',
	    );
	    $fields = array(
	        'format' => 'json',
	    );
	    $url = 'http://bucket.glanacion.com/common/anexos/xml/horoscopo.json';
	    // Open connection
	    $ch = curl_init();
	    // Set the url, number of GET vars, GET data
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_POST, false);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    // Execute request
	    $result = curl_exec($ch);
	    // Close connection
	    curl_close($ch);
	    $result = str_replace('horoscopoCallbackJSON({', '', $result);
	    $result = str_replace('"});', '', $result);
	    $result = explode('","', $result);
	    // Zodiac Sign Process
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"2');
	    $ariesSign = array(
	    	'slug' => 'aries',
	        'nombre' => str_replace('nombre":"', '', $result[3]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[1]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[2]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[4]),
	        'amor' => str_replace('Amor":"', '', $result[5]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[6]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[7]),
	        'simbolo' => '♈'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"3');
	    $tauroSign = array(
	    	'slug' => 'tauro',
	        'nombre' => str_replace('nombre":"', '', $result[10]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[8]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[9]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[11]),
	        'amor' => str_replace('Amor":"', '', $result[12]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[13]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[14]),
	        'simbolo' => '♉'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"4');
	    $geminisSign = array(
	    	'slug' => 'geminis',
	        'nombre' => str_replace('nombre":"', '', $result[17]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[15]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[16]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[18]),
	        'amor' => str_replace('Amor":"', '', $result[19]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[20]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[21]),
	        'simbolo' => '♊'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"5');
	    $cancerSign = array(
	    	'slug' => 'cancer',
	        'nombre' => str_replace('nombre":"', '', $result[24]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[22]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[23]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[25]),
	        'amor' => str_replace('Amor":"', '', $result[26]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[27]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[28]),
	        'simbolo' => '♋'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"6');
	    $leoSign = array(
	    	'slug' => 'leo',
	        'nombre' => str_replace('nombre":"', '', $result[31]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[29]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[30]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[32]),
	        'amor' => str_replace('Amor":"', '', $result[33]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[34]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[35]),
	        'simbolo' => '♌'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"7');
	    $virgoSign = array(
	    	'slug' => 'virgo',
	        'nombre' => str_replace('nombre":"', '', $result[38]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[36]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[37]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[39]),
	        'amor' => str_replace('Amor":"', '', $result[40]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[41]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[42]),
	        'simbolo' => '♍'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"8');
	    $libraSign = array(
	    	'slug' => 'libra',
	        'nombre' => str_replace('nombre":"', '', $result[45]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[43]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[44]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[46]),
	        'amor' => str_replace('Amor":"', '', $result[47]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[48]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[49]),
	        'simbolo' => '♎'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"9');
	    $escorpioSign = array(
	    	'slug' => 'escorpio',
	        'nombre' => str_replace('nombre":"', '', $result[52]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[50]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[51]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[53]),
	        'amor' => str_replace('Amor":"', '', $result[54]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[55]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[56]),
	        'simbolo' => '♏'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"10');
	    $sagitarioSign = array(
	    	'slug' => 'sagitario',
	        'nombre' => str_replace('nombre":"', '', $result[59]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[57]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[58]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[60]),
	        'amor' => str_replace('Amor":"', '', $result[61]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[62]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[63]),
	        'simbolo' => '♐'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"11');
	    $capricornioSign = array(
	    	'slug' => 'capricornio',
	        'nombre' => str_replace('nombre":"', '', $result[66]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[64]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[65]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[67]),
	        'amor' => str_replace('Amor":"', '', $result[68]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[69]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[70]),
	        'simbolo' => '♑'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}},{"signo":{"id":"12');
	    $aquarioSign = array(
	    	'slug' => 'acuario',
	        'nombre' => str_replace('nombre":"', '', $result[73]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[71]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[72]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[74]),
	        'amor' => str_replace('Amor":"', '', $result[75]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[76]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[77]),
	        'simbolo' => '♒'
	    );
	    $cleanJsonBien = array('Bienestar":"', '"}}]})');
	    $piscisSign = array(
	    	'slug' => 'piscis',
	        'nombre' => str_replace('nombre":"', '', $result[80]),
	        'desde' => substr_replace(str_replace('fechaDesde":"', '', $result[78]),'/',2,0),
	        'hasta' => substr_replace(str_replace('fechaHasta":"', '', $result[79]),'/',2,0),
	        'hoy' => str_replace('Hoy":"', '', $result[81]),
	        'amor' => str_replace('Amor":"', '', $result[82]),
	        'riqueza' => str_replace('Riqueza":"', '', $result[83]),
	        'bienestar' => str_replace($cleanJsonBien, '', $result[84]),
	        'simbolo' => '♓'
	    );


	    $zodiacSigns = array( $ariesSign, 
	    	$tauroSign, 
	    	$geminisSign, 
	    	$cancerSign, 
	    	$leoSign, 
	    	$virgoSign,
	        $libraSign,
	        $escorpioSign,
	        $sagitarioSign,
	        $capricornioSign,
	        $aquarioSign,
	        $piscisSign         
	    );
	    echo json_encode($zodiacSigns);
	}

}