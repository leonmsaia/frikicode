<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Commercial_model');
		$this->load->model('Internal_model');
		$this->load->model('User_model');
		$this->load->model('Location_Model');
	}

	public function index()
	{
		// Render Info for Toolbar
		$data['services'] = $this->Internal_model->getServices();
		$data['products'] = $this->Commercial_model->getProds();
		// Render Info for Toolbar End
		
		// Render for Package Detail
		// Render Title and Tags
		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Contacto';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Render Visualizations
		$data['titleSpot'] = 'Contacto';
		$data['subtextSpot'] = 'Envianos tu consulta, tu idea, tu problema y nosotros nos vamos a encargar de convertir eso en algo real, poseemos diferentes canales de comunicacion, todos orientados a dar el mejor servicio de pre-venta, venta, asesoria y post-venta.';

		$data['imageSpot'] = 'contact.jpeg';
		$data['videoSpot'] = '';
		// Render Visualizations End

		// Get Feed from Wordpress
		$data['blogUrl'] = $this->config->item('blog_url');
		$url = $data['blogUrl'] . 'feed/feedname';
		$data['rss'] = getNews($url);
		// Get Feed from Wordpress End

		// Load View
		$this->load->view('contact/contact', $data);
	}

	public function contactSent() {
		$message =  'Nombre: ' . $_POST['name'] . '<br>' .
	    			'Email: ' . $_POST['email'] . '<br>' .
				    'Mensaje: ' . $_POST['message'] . '<br>';
				    
		$userMail = $_POST['email'];

		$MailNfo = array(
            'sendto' => $userMail,
            'message' =>  $message,
        );
        $this->startMailProtocole($MailNfo);
	}

	function startMailProtocole($MailNfo)
	{
		$this->db->from('smtp_mail_conf');
		$this->db->where('smtp_conf_id', 1);
		$smtp = $this->db->get();
		foreach ($smtp->result() as $row) {
			$from = $row->from;
		}

		$config['protocol']    = 'smtp';
		$config['smtp_host']    = getMailConfiguration()['smtp_host'];
		$config['smtp_port']    = getMailConfiguration()['smtp_port'];
		$config['smtp_timeout'] = getMailConfiguration()['smtp_timeout'];
		$config['smtp_user']    = getMailConfiguration()['smtp_user'];
		$config['smtp_pass']    = getMailConfiguration()['smtp_pass'];
		$config['charset']    = getMailConfiguration()['smtp_charset'];
		$config['newline']    = getMailConfiguration()['smtp_newline'];
		$config['mailtype'] = getMailConfiguration()['smtp_mailtype'];
		$config['validation'] = getMailConfiguration()['smtp_validation'];
		
	    for ($i = 0; $i < 1; $i++) {
	        $subject = 'Consulta Web';
	        $sendto = getSiteConfiguration()['site_mail'];
	        $this->email->initialize($config);
	        $this->email->from($from, getSiteConfiguration()['site_name']);
	        //$this->email->to($sendto);
	        $this->email->to('leonmsaia@gmail.com');
	        $this->email->reply_to($from, getSiteConfiguration()['site_name']);
	        $this->email->subject($subject);
	        $this->email->message($MailNfo['message']);
	        $this->email->send();
	        echo $this->email->print_debugger();
	        $i + 1;
	    }
	    
	}

}