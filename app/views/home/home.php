<!DOCTYPE html>
<html lang="<?php echo $language;?>">
    <head>
        <?php $this->load->view('assets/headnfo');?>
        <?php $this->load->view('assets/style');?>
        <?php $this->load->view('assets/topscripts');?>
    </head>
    <body>
        <div class="wrapper">
        <?php $this->load->view('elements/header');?>
        <?php $this->load->view('elements/hero-module');?>
            <main class="container">
                <?php $this->load->view('elements/about-module');?>
                <?php $this->load->view('elements/services-module');?>
                <?php $this->load->view('elements/portfolio-module');?>
                <?php $this->load->view('elements/testimonial-module');?>
                <?php $this->load->view('elements/team-module');?>
                <?php $this->load->view('elements/blog-module');?>
                <?php $this->load->view('elements/subscription-module');?>
                <?php $this->load->view('elements/contact-module');?>
                <?php $this->load->view('elements/customer-module');?>
            </main>
            <a href="#" class="back-top btn">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        <?php $this->load->view('elements/footer');?>
        </div>
        <?php $this->load->view('assets/scripts');?>
    </body>
</html>