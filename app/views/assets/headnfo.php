<meta charset="<?php echo $charset;?>" />
<!-- <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $appleicon;?>"> -->
<!-- <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $favicon;?>"> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><?php echo $title;?></title>
<meta name="viewport" content="width=device-width" />
<meta name="description" content="<?php echo $description;?>">
<meta name="keywords" content="<?php echo $keywords;?>">
<meta name="author" content="<?php echo $author;?>">