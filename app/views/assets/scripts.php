<script type="text/javascript" src='<?php echo base_url();?>assets/js/jquery-2.1.3.min.js'></script>
<script type="text/javascript" src='<?php echo base_url();?>assets/js/plugins.min.js'></script>
<script type="text/javascript" src='<?php echo base_url();?>assets/js/bootstrap.min.js'></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_6tue0EjhSEmvxC0BG251N6w6fYh7r5s"></script>
<script type="text/javascript" src='<?php echo base_url();?>assets/js/main.js'></script>
<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
<script src="https://cdn.ywxi.net/js/inline.js?w=120"></script>
<script>
$('#contactForm').submit(function(event) {
      event.preventDefault();
      var $form = $(this);            
      var urlForm = $form.attr('action');
      var data = {
          name: $('#name').val(),
          email: $('#email').val(),
          message: $('#message').val()
      };
      $('#loadingDiv').show();
      $.post(urlForm, data,
      function(data){
      })
      .done(function() {
        $('#loadingDiv').hide();
        $('#contactOk').css('display', 'block');
        $('#contactSend').css('background-color', '#636363');
        $('#contactSend').prop('disabled', true);
      })
      .fail(function() {
        $('#loadingDiv').hide();
        $('#contactError').css('display', 'block');
        $('#contactSend').prop('disabled', true);
      });  
});

$('#mailChimp').submit(function(event) {
      event.preventDefault();
      var $form = $(this);            
      var urlForm = $form.attr('action');
      var data = {
          email: $('#email').val()
      };
      $('#loadingDiv').show();
      $.post(urlForm, data,
      function(data){
      })
      .done(function() {
        $('#mailchimpYes').css('display', 'block');
        $('#mc-embedded-subscribe').css('background-color', '#636363');
        $('#mc-embedded-subscribe').prop('disabled', true);
      })
      .fail(function() {
        $('#mailchimpNo').css('display', 'block');
        $('#mc-embedded-subscribe').prop('disabled', true);
      });  
});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56833505-1', 'auto');
  ga('send', 'pageview');

</script>