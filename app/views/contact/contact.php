<!DOCTYPE html>
<html lang="<?php echo $language;?>">
    <head>
        <?php $this->load->view('assets/headnfo');?>
        <?php $this->load->view('assets/style');?>
        <?php $this->load->view('assets/topscripts');?>
    </head>
    <body>
        <div class="wrapper">
        <?php $this->load->view('elements/header');?>
        <?php $this->load->view('elements/hero-module');?>
            <main class="container">
                <section class="ms-contact">
                    <div class="row">
                        <div class="ms-title">
                            <h2><?php echo $titleSpot;?></h2>
                            <h3><?php echo $subtextSpot;?></h3>
                        </div>
                        <div class="contact-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-6 info info-horizontal">
                                        <div class="description">
                                            <p><?php echo getContactNfo()['adress'];?></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 info info-horizontal">
                                        <div class="description">
                                            <p><?php echo getContactNfo()['phone'];?></p>
                                            <p><?php echo getContactNfo()['email'];?></p>
                                        </div>
                                    </div>
                                    <div class="info info-horizontal">
                                        <div class="description">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="contact-form">
                                    <form role="form" id="contactForm" action="<?php echo base_url();?>Contact/contactSent" method="post" accept-charset="utf-8">
                                        <div class="form-group col-md-6">
                                            <label class="control-label">Tu Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control">
                                        <span class="material-input"></span></div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label">Direccion de E-Mail</label>
                                            <input type="email" name="email" id="email" class="form-control">
                                        <span class="material-input"></span></div>
                                        <div class="form-group col-md-12">
                                            <label class="control-label">Tu Mensaje</label>
                                            <textarea name="message" class="form-control" id="message" rows="6"></textarea>
                                        <span class="material-input"></span></div>
                                        <div class="submit text-center col-md-12">
                                            <button type="submit" id="contactSend" class="btn btn-primary btn-raised btn-round" data-delay="1000">Contactanos</button>
                                        </div>
                                        <div id="loadingDiv" style="width:100%;float:left;display:none;">
                                            <img src="<?php echo base_url();?>assets/images/loader.gif"> Enviando....
                                        </div>
                                        <br>
                                        <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;float: left;width: 100%;margin-top: 10px;">
                                            <i class="material-icons">done</i> Su consulta se envio exitosamente
                                        </span>
                                        <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;float: left;width: 100%;margin-top: 10px;">
                                            <i class="material-icons">error</i> Se produjo un error
                                        </span>
                                    </form>
                                </div>
                           </div>
                        </div>
                    </div>
                    <div class="contact-map">
                        <div id="map" class="gmap3"></div>
                    </div>
                </section>
            </main>
            <script>
                  function initMap() {
                    if ($.exists('#map')) {
                      $('#map').gmap3({
                          address: "<?php echo getContactNfo()['adress'];?>",
                          zoom: 14,
                          scrollwheel: false,
                          mapTypeId : google.maps.MapTypeId.ROADMAP,
                          styles: [{
                            featureType: "",
                            elementType: "",
                            stylers: [{
                                saturation: -80
                            },{
                                lightness: 30
                            },{
                                hue: "#4E4691"
                            },{
                                visibility: "simplified"
                            }]
                        }],

                        }).marker(function (map) {
                          return {
                            position: [<?php echo getContactNfo()['lat'];?>,<?php echo getContactNfo()['lng'];?>],
                            icon: 'assets/images/ic_location.svg',
                          };
                        });
                    }
                  }
            </script>
            <a href="#" class="back-top btn">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        <?php $this->load->view('elements/footer');?>
        </div>
        <?php $this->load->view('assets/scripts');?>
    </body>
</html>