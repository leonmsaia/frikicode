<nav class="header row auto-hide-header navbar-transparent">
    <div class="header-inner">
        <div class="logo col-xs-2 col-md-2">
            <a href="<?php echo base_url();?>">
                <img src="<?php echo base_url();?>assets/images/logo.svg" alt="logo">
            </a>
        </div>
        <div class="mobile-menu-btn col-sm-2 col-md-2">
            <a href ="#" class="nav-trigger">
                <span><em aria-hidden="true"></em></span>
            </a>
        </div>
        <div class="menu col-md-8 col-xs-10" id="menu">
            <ul class="menu_list">
                <li>
                    <a href="<?php echo base_url();?>">Home</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>about">Nosotros</a>
                </li>
                <li>
                    <a href="<?php echo $blogUrl;?>">Blog</a>
                </li>
                <li><a href="<?php echo base_url();?>services">Servicios</a>
                    <ul class="sub-menu">
                        <?php foreach ($services->result() as $serv): ?>
                            <li><a href="<?php echo base_url();?>services/<?php echo $serv->slug;?>"><?php echo $serv->title;?></a></li>
                        <?php endforeach ?>
                    </ul>
                </li>
                <li><a href="<?php echo base_url();?>products">Productos</a>
                    <ul class="sub-menu">
                        <?php foreach ($products->result() as $prods): ?>
                            <li><a href="<?php echo base_url();?>products/<?php echo $prods->slug;?>"><?php echo $prods->name;?></a></li>
                        <?php endforeach ?>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url();?>works">Works</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>partners">Partners</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>contact">Contacto</a>
                </li>
           </ul>
        </div>
    </div>
</nav>