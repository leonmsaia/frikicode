<section class="ms-blog ghost-bg">
    <div class="row">
        <div class="ms-title">
            <h2>Ultimas Noticias del Blog</h2>
            <h3>Enterate de las ultimas noticias del mundo de la Informatica, la Cultura Pop, descubri nuevos tips de desarrollo, conoce nuestras novedades, sorteos y cursos via streaming.</h3>
        </div>
        <ul class="ms-grid">
            <?php foreach ($rss as $rs): ?>
                <li class="blog-post col-md-4">
                    <a href="<?php echo $rs['link'];?>">
                        <img src="<?php echo $rs['thumbnail'];?>" alt="<?php echo $rs['title'];?>">
                        <h5><?php echo $rs['title'];?></h5>
                    </a>
                    <div class="post-info">
                        <a href="<?php echo $blogUrl . '/category/' . $rs['categoryslug'];?>"><i class="material-icons">bookmark_border</i><?php echo $rs['categoryname'];?></a>
                        <a href="#"><i class="material-icons">date_range</i><?php echo $rs['pubDate'];?></a>
                        <a href="#"><i class="material-icons">chat_bubble_outline</i><?php echo $rs['comments'];?></a>
                    </div>
                </li>
            <?php endforeach ?>
        </ul>
        <div class="section-button col-md-12">
            <a href="<?php echo $blogUrl;?>" class="btn btn-primary">Ver Blog</a>
        </div>
    </div>
</section>