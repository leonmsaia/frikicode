<section class="ms-services ghost-bg" id="ms-services">
    <div class="row">
        <div class="ms-title">
            <h2>Servicios</h2>
            <h3>Disponemos una amplia gama de servicios para poner a tu disposicion, nos especializamos en soluciones punta a punta en Desarrollo de Sistemas A Medida.</h3>
        </div>
        <article>
            <?php foreach ($services->result() as $srvs): ?>
                <div class="service-item col-md-4">
                    <i class="material-icons" style="color:<?php echo $srvs->color;?>;"><?php echo $srvs->icon;?></i>
                    <div class="services-content">
                        <h4><?php echo $srvs->title;?></h4>
                        <p><?php echo $srvs->short_desc;?></p>
                        <a href="<?php echo base_url() . 'services/' . $srvs->slug; ?>" class="btn">Conocer Mas</a>
                    </div>
                </div>
            <?php endforeach ?>
        </article>
    </div>
</section>