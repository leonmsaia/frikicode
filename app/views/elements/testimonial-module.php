<section class="ms-testimonial transparent-bg parallax-slider" style="background-image: url(assets/images/hero-bg.jpg)">
    <div class="row">
        <div class="ms-title">
            <h2>¿Porque Nuestros Clientes nos Adoran?</h2>
            <h3>Estas son algunas de las referencias dejadas por nuestros queridos clientes.</h3>
        </div>
        <div class="testimonial-block col-md-12">
            <div class="controls-navigate">
                <div class="prev">
                    <i class="material-icons">keyboard_arrow_left</i>
                </div>
                <div class="next">
                    <i class="material-icons">keyboard_arrow_right</i>
                </div>
            </div>
            <div class="testimonial-slider">
                <?php foreach ($testimonial->result() as $tst): ?>
                    <?php if ($tst->approved == 1): ?>
                        <div class="testimonial-cell">
                            <div class="testimonial-quote card-raised">
                                <q><?php echo $tst->comment;?></q>
                            </div>
                            <div class="testimonial-author">
                                <div class="author-content">
                                    <div class="testimonial-avatar">
                                        <i class="material-icons">bubble_chart</i>
                                        <img src="assets/uploads/files/testimonials/<?php echo $tst->pic;?>" alt="<?php echo $tst->name;?>">
                                    </div>
                                    <p><?php echo $tst->name;?>,<span><?php echo $tst->position;?></span></p>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</section>