<div class="ms-customer">
    <div class="row">
        <ul class="customers-list">
            <?php $techs = getTechs();?>
            <?php foreach ($techs->result() as $thc): ?>
                <li><a href="<?php echo $thc->website;?>"><img src="<?php echo base_url();?>assets/uploads/files/partner/<?php echo $thc->logo;?>" alt="logo"></a></li>
            <?php endforeach ?>
        </ul>
    </div>
</div>