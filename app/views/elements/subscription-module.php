<section class="ms-subscribe parallax-slider" style="background-image: url(assets/images/hero-bg.jpg)">
    <div class="row">
        <div class="ms-title">
            <h2>Subscribite a nuestro boletin de novedades</h2>
            <h3>Conoce todos los dias novedades del mundo de la Informatica, totalmente gratis!</h3>
        </div>
        <div class="subscribe-content col-md-12">
            <div class="card-raised col-md-6">
                <form id="mailChimp" name="mailChimp" action="<?php echo base_url();?>Home/mailChimpAdd" method="post" accept-charset="utf-8">
                    <div class="form-group col-md-8">
                    <span class="input-group-addon">
                        <i class="material-icons">mail</i>
                    </span>
                        <input id="emailChimpMail" type="email" name="emailChimpMail" placeholder="Ingresa tu E-Mail"  class="form-control" autocomplete="off" required>
                    <span class="material-input"></span>
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="submit">
                            <input type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary btn-raised btn-round" value="Subscribirme">
                        </div>
                    </div>
                </form>
            </div>
            <div id="mailchimpYes" class="alert alert-success" role="alert" style="display:none;">Te agregamos correctamente a nuestro grupo! Por favor, verifica tu Mail</div>
            <div id="mailchimpNo" class="alert alert-danger" role="alert" style="display:none;">Algo ocurrio...</div>
        </div>
    </div>
</section>