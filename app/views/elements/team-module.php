<section class="ms-team">
    <div class="row">
        <div class="ms-title">
            <h2>Nuestro Equipo</h2>
            <h3>Conoce a nuestro equipo de profesionales de IT, nuestro recurso mas preciado. Dia a dia nos perfeccionamos como seres humanos y como profesionales. Nuestro objetivo es desarrollador metodologias al ritmo de los nuevos desarrollos.</h3>
        </div>
        <div class="ms-our-team ms-grid">
            <?php $counter = 0;?>
            <?php foreach ($crew->result() as $crw): ?>
                <?php if ($counter <= 4): ?>
                    <figure class="grid-item col-md-3 col-sm-6">
                        <img src="<?php echo base_url();?>assets/uploads/files/crew/<?php echo $crw->pic;?>" alt="worker-image">
                        <figcaption>
                            <p><?php echo $crw->name . ' ' . $crw->lastname;?><small><?php echo $crw->position;?></small></p>
                            <p><?php echo $crw->bio;?></p>
                            <div class="team-socials">
                                <a href="<?php echo $crw->twitter;?>" target="_blank" class="btn btn-just-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                                <a href="<?php echo $crw->facebook;?>" target="_blank" class="btn btn-just-icon btn-facebook"><i class="fa fa-facebook"></i></a>
                                <a href="<?php echo $crw->instagram;?>" target="_blank" class="btn btn-just-icon btn-instagram"><i class="fa fa-instagram"></i></a>
                                <a href="<?php echo $crw->linkedin;?>" target="_blank" class="btn btn-just-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </figcaption>
                    </figure>
                    <?php $counter++;?>
                <?php endif ?>
            <?php endforeach ?>
        </div>
        <div class="section-button col-md-12">
            <a href="<?php echo base_url() . 'about/';?>" class="btn btn-primary">Ver Todo el Equipo</a>
        </div>
    </div>
</section>