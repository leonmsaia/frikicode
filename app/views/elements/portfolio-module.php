<section class="ms-portfolio">
    <div class="row">
        <div class="ms-title">
            <h2>Trabajos Realizados</h2>
            <h3>Conoce algunos de nuestros mejores trabajos, verifica hoy mismo la calidad de nuestro codigo y comparti nuestra vision.</h3>
        </div>
        <div class="ms-grid">
            <?php $limit = 1;?>
            <?php foreach ($works->result() as $wrks): ?>
                <?php if ($limit <= 6): ?>
                    <figure class="grid-item col-md-4">
                        <div class="item-content">
                            <h4 style="text-align:center;"><?php echo $wrks->name;?></h4>
                            <img src="<?php echo base_url();?>assets/uploads/files/work/<?php echo $wrks->pic1;?>" alt="work-image">
                            <div class="item-select-option">
                                <a class="image-link" href="<?php echo base_url();?>assets/uploads/files/work/<?php echo $wrks->pic1;?>" data-effect="mfp-zoom-in">
                                    <i class="material-icons">zoom_out_map</i>
                                </a>
                                <a class="work-link" href="<?php echo base_url() . 'works/' . $wrks->slug;?>">
                                    <i class="material-icons">link</i>
                                </a>
                            </div>
                        </div>
                        <figcaption>
                        <a href="#">
                            <p><?php echo $wrks->short_desc;?></p>
                        </a>
                        </figcaption>
                    </figure>
                <?php endif ?>
                <?php $limit++;?>
            <?php endforeach ?>
        </div>
        <div class="section-button col-md-12">
            <a href="<?php echo base_url() . 'works/' ?>" class="btn btn-primary">ver trabajos</a>
        </div>
    </div>
</section>