<section class="hero-parallax parallax-slider" style="background-image: url(assets/images/hero-bg.jpg)">
    <div class="ms-hero-copy ms-title">
        <h1><?php echo $titleSpot;?></h1>
        <h3><?php echo $subtextSpot;?></h3>                 
    </div>
    <div class="ellipse-border">
        <svg version="1.1" id="circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 380" xml:space="preserve" PreserveAspectRatio="none">
        <path fill="#fff" d="M0,309c0,0,340,70.8,960,70.8c620,0.2,960-70.8,960-70.8v71H0" transform="translate(0 1)"/>
        </svg>
    </div>
    <?php if ($imageSpot != null): ?>
        <img id="bgvid" src="assets/images/static/<?php echo $imageSpot;?>">
    <?php endif ?>
    <?php if ($videoSpot != null): ?>
        <video id="bgvid" playsinline autoplay muted loop>
    		<source src="<?php echo base_url();?>assets/videos/<?php echo $videoSpot;?>" type="video/mp4">
    	</video>
    <?php endif ?>
</section>