<footer id="footer" class="ghost-bg">
    <div class="ellipse-border-bottom">
        <svg version="1.1" id="circle2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 71.6" xml:space="preserve">
        <path fill="#fff" class="st0"
        d="M1919.9,71.6c0,0-344.1-65.2-964.1-65.2C335.9,6.2-0.1,71.6-0.1,71.6l0-72.2l958.9,0l961.1,0"/>
        </svg>
    </div>
    <div class="row">
        <ul class="footer-social-buttons">
        <?php
            $linkedinLink = getContactNfo()['linkedin'];
            $facebookLink = getContactNfo()['facebook'];
            $twitterLink = getContactNfo()['twitter'];
        ?>
            <?php if ($twitterLink != null): ?>
                <li>
                    <a href="<?php echo $twitterLink;?>" target="_blank" class="btn btn-just-icon btn-simple btn-twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
            <?php endif ?>

            <?php if ($facebookLink != null): ?>
            <li>
                <a href="<?php echo $facebookLink;?>" target="_blank" class="btn btn-just-icon btn-simple btn-facebook">
                    <i class="fa fa-facebook-square"></i>
                </a>
            </li>
            <?php endif ?>

            <?php if ($linkedinLink != null): ?>
            <li>
                <a href="<?php echo $linkedinLink;?>" target="_blank" class="btn btn-just-icon btn-simple btn-linkedin">
                    <i class="fa fa-linkedin"></i>
                </a>
            </li>
            <?php endif ?>
        </ul>
        <ul class="certifications">
            <li>
                <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=b994kcfs7v2Jey16ZKGaTOYAXVnGKQv3G68Wv9rwIv4G8Jq2W8tXXmldlWUx"></script></span>
            </li>
            <li>
                <a href="http://www.w3.org/html/logo/">
                    <img src="https://www.w3.org/html/logo/badge/html5-badge-h-connectivity-css3-device-graphics-multimedia-performance-semantics-storage.png" width="357" height="64" alt="HTML5 Powered with Connectivity / Realtime, CSS3 / Styling, Device Access, Graphics, 3D &amp; Effects, Multimedia, Performance &amp; Integration, Semantics, and Offline &amp; Storage" title="HTML5 Powered with Connectivity / Realtime, CSS3 / Styling, Device Access, Graphics, 3D &amp; Effects, Multimedia, Performance &amp; Integration, Semantics, and Offline &amp; Storage">
                </a>
            </li>
            <li>
                <div style="width:97px;">
                    <a rel="nofollow" href="http://www.qweb.es/_agencias-de-diseno-web.html" target="_blank" title="Directorio de Agencias de diseño web">
                    <img src="http://www.qweb.es/certqweb-friki-code.com.gif" width="97" height="31" style="border:0" alt="Directorio de Agencias de diseño web" />
                    </a>
                </div>
            </li>
        </ul>
        <div class="copyright">
            Copyright © <script>document.write(new Date().getFullYear())</script> FrikiCode. Todos los Derechos Reservados.
        </div>
    </div>
</footer>