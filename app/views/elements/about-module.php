<section class="ms-about content" id="ms-about">
    <div class="row">
        <div class="ms-title">
            <h2>Acerca de Nosotros</h2>
        </div>
        <div class="col-md-8 col-md-push-2">
        	<?php foreach ($about->result() as $abt): ?>
	            <?php echo $abt->text;?>
        	<?php endforeach ?>
        </div>
    </div>
</section>