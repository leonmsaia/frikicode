<!DOCTYPE html>
<html lang="<?php echo $language;?>">
    <head>
        <?php $this->load->view('assets/headnfo');?>
        <?php $this->load->view('assets/style');?>
        <?php $this->load->view('assets/topscripts');?>
    </head>
    <body>
        <div class="wrapper">
        <?php $this->load->view('elements/header');?>
        <?php $this->load->view('elements/hero-module');?>
            <main class="container">
                <section class="ms-team">
                    <div class="row">
                        <div class="ms-title">
                            <h2><?php echo $titleSpot;?></h2>
                            <h3><?php echo $subtextSpot;?></h3>
                        </div>
                        <div class="ms-portfolio">
                            <div class="row">
                                <div class="ms-grid">
                                    <?php foreach ($works->result() as $wrks): ?>
                                        <figure class="grid-item col-md-3">
                                            <h4 style="text-align:center;"><?php echo $wrks->name;?></h4>
                                            <div class="item-content">
                                                <img src="<?php echo base_url();?>assets/uploads/files/work/<?php echo $wrks->pic1;?>" alt="<?php echo $wrks->name;?>">
                                                <div class="item-select-option">
                                                    <a class="image-link" href="<?php echo base_url();?>assets/uploads/files/work/<?php echo $wrks->pic1;?>" data-effect="mfp-zoom-in">
                                                        <i class="material-icons">zoom_out_map</i>
                                                    </a>
                                                    <a class="work-link" href="<?php echo base_url() . 'works/' . $wrks->slug;?>">
                                                        <i class="material-icons">link</i>
                                                    </a>
                                                </div>
                                            </div>
                                            <figcaption>
                                            <a href="#">
                                                <p>Categoria</p>
                                            </a>
                                            </figcaption>
                                        </figure>
                                    <?php endforeach ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
            <a href="#" class="back-top btn">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        <?php $this->load->view('elements/footer');?>
        </div>
        <?php $this->load->view('assets/scripts');?>
    </body>
</html>