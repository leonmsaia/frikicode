<!DOCTYPE html>
<html lang="<?php echo $language;?>">
    <head>
        <?php $this->load->view('assets/headnfo');?>
        <?php $this->load->view('assets/style');?>
        <?php $this->load->view('assets/topscripts');?>
    </head>
    <body>
        <div class="wrapper">
        <?php $this->load->view('elements/header');?>
        <?php $this->load->view('elements/hero-module');?>
           <main class="container">
                <section class="ms-team">
                    <div class="row">
                        
                        <div class="ms-title">
                            <h2>Porfolio personal de <?php echo $titleSpot;?></h2>
                            <h6>Powered by <i class="fa fa-behance"></i></h6>
                        </div>
                        <?php foreach ($personalPorfolio as $prf): ?>
                        <?php $date = gmdate('d/m/Y', $prf['published_on']);?>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <p>
                                            <b>Cliente:</b> <?php echo $prf['name'];?> <br>
                                            <b>Categoria:</b> <?php echo $prf['fields'][0];?> <br>
                                            <b>Fecha de Publicacion:</b> <?php echo $date;?> <br>
                                        </p>
                                        <a class="btn btn-primary" href="<?php echo $prf['url'];?>" target="_blank" alt="<?php echo $prf['name'];?>">Ver Muestra en Behance</a>
                                    </div>
                                    <div class="imageContainer col-md-7">
                                        <img class="img-responsive" src="<?php echo $prf['covers']['original'];?>" alt="<?php echo $prf['name'];?>">
                                    </div>
                                </div>
                            <hr>
                            </div>
                        <?php endforeach ?>
                        <br>
                        <div class="col-md-12">
                            <h5>Importante</h5>
                            <p>
                                Este porfolio es personal del artista de FrikiCode, los trabajos aqui expuestos que no se encuentren en la seccion "Works" no son produccion de FrikiCode.
                            </p>
                        </div>
                    </div>
                </section>
            </main>
            <a href="#" class="back-top btn">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        <?php $this->load->view('elements/footer');?>
        </div>
        <?php $this->load->view('assets/scripts');?>
        <script>
            $('.hero-parallax.parallax-slider').css('height','300px');
        </script>
    </body>
</html>