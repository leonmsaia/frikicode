<!DOCTYPE html>
<html lang="<?php echo $language;?>">
    <head>
        <?php $this->load->view('assets/headnfo');?>
        <?php $this->load->view('assets/style');?>
        <?php $this->load->view('assets/topscripts');?>
    </head>
    <body>
        <div class="wrapper">
        <?php $this->load->view('elements/header');?>
        <?php $this->load->view('elements/hero-module');?>
            <main class="container">
                <section class="ms-services ghost-bg" id="ms-services">
                    <div class="row">
                        <div class="ms-title">
                            <h2><?php echo $titleSpot;?></h2>
                            <h3><?php echo $subtextSpot;?></h3>
                        </div>
                        <article>
                                
                            <?php foreach ($services->result() as $serv): ?>
                                <div class="service-item col-md-4">
                                    <i class="material-icons" style="color:<?php echo $serv->color;?>;"><?php echo $serv->icon;?></i>
                                    <div class="services-content">
                                        <h4><?php echo $serv->title;?></h4>
                                        <p><?php echo $serv->short_desc;?></p>
                                        <a href="<?php echo base_url() . 'services/' . $serv->slug; ?>" class="btn">Conocer Mas</a>
                                    </div>
                                </div>
                            <?php endforeach ?>
                            
                        </article>
                    </div>
                </section>
            </main>
            <a href="#" class="back-top btn">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        <?php $this->load->view('elements/footer');?>
        </div>
        <?php $this->load->view('assets/scripts');?>
    </body>
</html>