<!DOCTYPE html>
<html lang="<?php echo $language;?>">
    <head>
        <?php $this->load->view('assets/headnfo');?>
        <?php $this->load->view('assets/style');?>
        <?php $this->load->view('assets/topscripts');?>
    </head>
    <body>
        <div class="wrapper">
        <?php $this->load->view('elements/header');?>
        <?php $this->load->view('elements/hero-module');?>
            <main class="container">
                <section class="ms-about content" id="ms-about">
                    <div class="row">
                        <div class="ms-title">
                            <h2><?php echo $titleSpot;?></h2>
                        </div>
                        <div class="col-md-8 col-md-push-2">
                          <p><?php echo $descSpot;?></p>
                          <p><?php echo $methodSpot;?></p>
                        </div>
                    </div>
                </section>
            </main>
            <a href="#" class="back-top btn">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        <?php $this->load->view('elements/footer');?>
        </div>
        <?php $this->load->view('assets/scripts');?>
    </body>
</html>