<!DOCTYPE html>
<html lang="<?php echo $language;?>">
    <head>
        <?php $this->load->view('assets/headnfo');?>
        <?php $this->load->view('assets/style');?>
        <?php $this->load->view('assets/topscripts');?>
    </head>
    <body>
        <div class="wrapper">
        <?php $this->load->view('elements/header');?>
        <?php $this->load->view('elements/hero-module');?>
            <main class="container">
                <section class="ms-team">
                    <div class="row">
                        
                        <div class="ms-title">
                            <h2><?php echo $titleSpot;?></h2>
                            <?php foreach ($about->result() as $abt): ?>
                                <h3><?php echo $abt->text;?></h3>
                            <?php endforeach ?>
                        </div>

                        <div class="ms-our-team ms-grid">

                            <?php foreach ($crew->result() as $crw): ?>
                                <figure class="grid-item col-md-3 col-sm-6">
                                    <img src="<?php echo base_url();?>assets/uploads/files/crew/<?php echo $crw->pic;?>" alt="worker-image">
                                    <figcaption>
                                        <p><?php echo $crw->name . ' ' . $crw->lastname;?><small><?php echo $crw->position;?></small></p>
                                        <p><?php echo $crw->bio;?></p>
                                        <div class="team-socials">
                                            <?php if ($crw->twitter != null): ?>
                                                <a href="<?php echo $crw->twitter;?>" target="_blank" class="btn btn-just-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                                            <?php endif ?>
                                            
                                            <?php if ($crw->facebook != null): ?>
                                                <a href="<?php echo $crw->facebook;?>" target="_blank" class="btn btn-just-icon btn-facebook"><i class="fa fa-facebook"></i></a>
                                            <?php endif ?>

                                            <?php if ($crw->instagram != null): ?>
                                                <a href="<?php echo $crw->instagram;?>" target="_blank" class="btn btn-just-icon btn-instagram"><i class="fa fa-instagram"></i></a>
                                            <?php endif ?>

                                            <?php if ($crw->linkedin != null): ?>
                                                <a href="<?php echo $crw->linkedin;?>" target="_blank" class="btn btn-just-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>
                                            <?php endif ?>

                                            <?php if ($crw->behance != null): ?>
                                                <a href="<?php echo $crw->behance;?>" target="_blank" class="btn btn-just-icon btn-behance"><i class="fa fa-behance"></i></a>
                                            <?php endif ?>


                                        </div>
                                    </figcaption>
                                    <?php if ($crw->behanceUser != null): ?>
                                        <div>
                                            <a class="btn btn-primary" alt="<?php echo $crw->name;?>" href="<?php echo base_url();?>about/porfolio/<?php echo $crw->behanceUser;?>" style="width:100%;">
                                                Ver Porfolio Personal
                                            </a>
                                        </div>
                                    <?php endif ?>
                                </figure>
                            <?php endforeach ?>
                            
                        </div>
                    </div>
                </section>
            </main>
            <a href="#" class="back-top btn">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        <?php $this->load->view('elements/footer');?>
        </div>
        <?php $this->load->view('assets/scripts');?>
    </body>
</html>