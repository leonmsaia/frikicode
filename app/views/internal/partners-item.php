<!DOCTYPE html>
<html lang="<?php echo $language;?>">
    <head>
        <?php $this->load->view('assets/headnfo');?>
        <?php $this->load->view('assets/style');?>
        <?php $this->load->view('assets/topscripts');?>
    </head>
    <body>
        <div class="wrapper">
        <?php $this->load->view('elements/header');?>
        <?php $this->load->view('elements/hero-module');?>
            <main class="container">
                <section class="ms-about content" id="ms-about">
                    <div class="row">
                        <?php if ($imgSpot == null): ?>
                            <img class="img-responsive" src="<?php echo base_url();?>assets/uploads/files/partner/<?php echo $thumbSpot;?>" alt="<?php echo $thumbSpot;?>" style="width: 35%;margin: 0 auto;">
                        <?php else: ?>
                            <img class="img-responsive" src="<?php echo base_url();?>assets/uploads/files/partner/<?php echo $imgSpot;?>" alt="<?php echo $titleSpot;?>">
                        <?php endif ?>

                        <div class="ms-title">
                            <h2><?php echo $titleSpot;?></h2>
                        </div>
                        <div class="col-md-8 col-md-push-2">
                          <p><?php echo $descSpot;?></p>
                          <a href="<?php echo $urlSpot;?>" target="_blank" class="btn" style="width:35%;margin:45px auto;display:inherit;">Ver Sitio Oficial</a>
                        </div>
                    </div>
                </section>
            </main>
            <a href="#" class="back-top btn">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        <?php $this->load->view('elements/footer');?>
        </div>
        <?php $this->load->view('assets/scripts');?>
    </body>
</html>