<!DOCTYPE html>
<html lang="<?php echo $language;?>">
    <head>
        <?php $this->load->view('assets/headnfo');?>
        <?php $this->load->view('assets/style');?>
        <?php $this->load->view('assets/topscripts');?>
    </head>
    <body>
        <div class="wrapper">
        <?php $this->load->view('elements/header');?>
        <?php $this->load->view('elements/hero-module-work');?>
            <main class="container">

                <section>
                    <div class="row">
                        <div class="portfolio-inner">
                            <div class="col-md-8">
                                <?php if ($imgSpot1 != null): ?>
                                    <img class="img-responsive" src="<?php echo base_url() . 'assets/uploads/files/work/' . $imgSpot1;?>" alt="<?php echo $titleSpot;?>">
                                    <div class="spacer"></div>
                                <?php endif ?>
                                <?php if ($imgSpot2 != null): ?>
                                    <img class="img-responsive" src="<?php echo base_url() . 'assets/uploads/files/work/' . $imgSpot2;?>" alt="<?php echo $titleSpot;?>">
                                    <div class="spacer"></div>
                                <?php endif ?>
                                <?php if ($imgSpot3 != null): ?>
                                    <img class="img-responsive" src="<?php echo base_url() . 'assets/uploads/files/work/' . $imgSpot3;?>" alt="<?php echo $titleSpot;?>">
                                    <div class="spacer"></div>
                                <?php endif ?>
                            </div>                            
                            
                            <div class="col-md-4" id="sticky">
                                <h4>Nombre del Proyecto</h4>
                                <p><?php echo $titleSpot;?></p>
                                <h4>Descripcion del Proyecto</h4>
                                <p><?php echo $descSpot;?></p>
                                <h4>Detalles del Proyecto</h4>
                                <ul class="project-details">
                                    <li><span>Cliente: </span><?php echo $clientSpot;?></li>
                                    <li><span>Link: </span><a href="<?php echo $linkSpot;?>" target="_blank"><?php echo $linkSpot;?></a></li>
                                    <li><span>Categoria: </span><?php echo getWorkCatNfo($categorySpot)['title'];?></li>
                                    <li><span>Fecha: </span><?php echo $dateSpot;?></li>
                                    <li class="share-post">
                                        <span>Compartir:</span>
                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="single-nav portfolio-item-nav">
                                
                                <?php foreach ($workPrev->result() as $wrkPrev): ?>
                                    <?php if ($workPrev != null): ?>
                                        <div class="single-prev">
                                            <span>
                                                <a href="<?php echo base_url() . 'works/' . $wrkPrev->slug;?>" class="btn">
                                                    <i class="material-icons">keyboard_arrow_left</i>
                                                </a>
                                            </span>
                                            <span class="single-next-title"><?php echo $wrkPrev->name;?></span>
                                        </div>
                                    <?php endif ?>
                                <?php endforeach ?>
                                
                                <div class="btn">
                                    <a href="<?php echo base_url() . 'works';?>">
                                        <i class="material-icons">view_module</i>
                                    </a>
                                </div>
                                <?php foreach ($workNext->result() as $wrkNxt): ?>
                                    <?php if ($workNext != null): ?>
                                        <div class="single-next">
                                            <span class="single-next-title"><?php echo $wrkNxt->name;?></span>
                                            <span>
                                                <a href="<?php echo base_url() . 'works/' . $wrkNxt->slug;?>" class="btn">
                                                    <i class="material-icons">keyboard_arrow_right</i>
                                                </a>
                                            </span>
                                        </div>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </div>
                        </div>
                        
                    </div>
                </section>

            </main>
            <a href="#" class="back-top btn">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        <?php $this->load->view('elements/footer');?>
        </div>
        <?php $this->load->view('assets/scripts');?>
    </body>
</html>