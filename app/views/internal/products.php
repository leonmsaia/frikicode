<!DOCTYPE html>
<html lang="<?php echo $language;?>">
    <head>
        <?php $this->load->view('assets/headnfo');?>
        <?php $this->load->view('assets/style');?>
        <?php $this->load->view('assets/topscripts');?>
    </head>
    <body>
        <div class="wrapper">
        <?php $this->load->view('elements/header');?>
        <?php $this->load->view('elements/hero-module');?>
            <main class="container">
                <section class="ms-team">
                    <div class="row">
                        <div class="ms-title">
                            <h2><?php echo $titleSpot;?></h2>
                            <h3><?php echo $subtextSpot;?></h3>
                        </div>
                        <div class="ms-our-team ms-grid">
                                
                            <?php foreach ($products->result() as $prod): ?>
                            <figure class="grid-item col-md-3 col-sm-6">
                                <figcaption>
                                    <p><?php echo $prod->name;?></p>
                                    <p><?php echo $prod->short_desc; ?></p>
                                    <div class="team-socials">
                                        <a href="<?php echo base_url() . 'products/' . $prod->slug; ?>" class="btn">Conocer Producto</a>
                                        <a href="#demo" class="btn">Ver Demo</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <?php endforeach ?>

                        </div>
                    </div>
                </section>
            </main>
            <a href="#" class="back-top btn">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        <?php $this->load->view('elements/footer');?>
        </div>
        <?php $this->load->view('assets/scripts');?>
    </body>
</html>