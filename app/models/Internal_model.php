<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Internal_model extends CI_Model
{
	// Internal Functions //
	// Internals Show
	public function getInternalBySlug($slug)
	{	
		$this->db->where('slug', $slug);
		$result = $this->db->get('internal_page');
		return $result;
	}
	public function getInternalALL()
	{
		$this->db->order_by('page_id', 'desc'); 
		$result = $this->db->get('internal_page');
		return $result;
	}
	public function getAllFaqs()
	{
		$this->db->order_by('faq_id', 'desc'); 
		$result = $this->db->get('faq');
		return $result;
	}
	public function getAllTestimonials()
	{
		$this->db->order_by('testimonial_id', 'desc'); 
		$result = $this->db->get('testimonial');
		return $result;
	}
	public function getAllAbouts()
	{
		$this->db->order_by('about_id', 'desc'); 
		$result = $this->db->get('about');
		return $result;
	}
	public function getAllPartners()
	{
		$this->db->order_by('partner_id', 'desc');
		$result = $this->db->get('partners');
		return $result;
	}
	public function getPartnerBySlug($slug)
	{	
		$this->db->where('slug', $slug);
		$result = $this->db->get('partners');
		return $result;
	}

	public function getServices()
	{
		$this->db->order_by('title', 'asc'); 
		$result = $this->db->get('services');
		return $result;
	}
	public function getServiceBySlug($slug)
	{	
		$this->db->where('slug', $slug);
		$result = $this->db->get('services');
		return $result;
	}
	public function getCrew()
	{
		$this->db->order_by('crew_id', 'asc'); 
		$result = $this->db->get('crew');
		return $result;
	}
	public function getCrewByBehanceName($behanceName)
	{
		$this->db->where('behanceUser', $behanceName);
		$this->db->order_by('crew_id', 'asc'); 
		$result = $this->db->get('crew');
		return $result;
	}
	// Internals Show End
}