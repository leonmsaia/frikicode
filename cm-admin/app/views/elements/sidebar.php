<div class="sidebar-wrapper">
            
    <div class="user">
        <div class="photo">
            <img src="<?php echo getUserNfo(getMyID())['image'];?>" />
        </div>  
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                <?php echo getUserNfo(getMyID())['first_name'];?> <?php echo getUserNfo(getMyID())['last_name'];?>
                <b class="caret"></b>
            </a>                
            <div class="collapse" id="collapseExample">
                <ul class="nav">
                    <li><a href="<?php echo base_url();?>user">Mi Perfil</a></li>
                    <li><a href="#">Configurar Cuenta</a></li>
                    <li><a href="<?=base_url()?>auth/logout">Cerrar Sesion</a></li>
                </ul>
            </div>
        </div>
    </div>
                          
    <ul class="nav">

        <li>                   
            <a href="<?php echo base_url();?>Crud/AboutView">
                <i class="pe-7s-network"></i> 
                <p>Quienes Somos</p>
            </a>                
        </li>
        
        <li>                   
            <a href="<?php echo base_url();?>Crud/ServicesView">
                <i class="pe-7s-network"></i> 
                <p>Servicios</p>
            </a>                
        </li>

        <li>                   
            <a href="<?php echo base_url();?>Crud/CrewView">
                <i class="pe-7s-network"></i> 
                <p>Crew</p>
            </a>                
        </li>

        <li>                   
            <a href="<?php echo base_url();?>Crud/PartnerView">
                <i class="pe-7s-network"></i> 
                <p>Partners</p>
            </a>                
        </li>

        <li>                   
            <a href="<?php echo base_url();?>Crud/TechView">
                <i class="pe-7s-network"></i> 
                <p>Tecnologias</p>
            </a>                
        </li>

        <li>                   
            <a href="<?php echo base_url();?>Crud/WorkView">
                <i class="pe-7s-network"></i> 
                <p>Work</p>
            </a>                
        </li>

        <li>                   
            <a href="<?php echo base_url();?>Crud/WorkCatView">
                <i class="pe-7s-network"></i> 
                <p>Work Categoria</p>
            </a>                
        </li>

        <li>                   
            <a data-toggle="collapse" href="#products" class="collapsed" aria-expanded="true">                        
                <i class="pe-7s-portfolio"></i> 
                <p>Catalogo
                   <b class="caret"></b>
                </p>
            </a>                
            <div class="collapse" id="products" aria-expanded="true">
                <ul class="nav">
                    <li>
                        <a href="<?php echo base_url();?>Crud/Product">Productos</a>
                    </li>
                </ul>
            </div>
        </li>

        <li>                   
            <a href="<?php echo base_url();?>Crud/testimonialView">
                <i class="pe-7s-users"></i> 
                <p>Testimoniales</p>
            </a>                
        </li>

        <li>                   
            <a href="<?php echo base_url();?>Crud/UserListView">
                <i class="pe-7s-users"></i> 
                <p>Usuarios</p>
            </a>                
        </li>

        <li>                   
            <a href="<?php echo base_url();?>Crud/MailConfView">
                <i class="pe-7s-map"></i> 
                <p>Configurar Mailing</p>
            </a>                
        </li>

        <li>                   
            <a href="<?php echo base_url();?>Crud/InfoContactConf">
                <i class="pe-7s-map"></i> 
                <p>Info. de Contacto</p>
            </a>                
        </li>

    </ul>  
</div>